package com.fr.adaming.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fr.adaming.dao.UserDao;
import com.fr.adaming.entity.User;

@RestController
@RequestMapping(path = "/user")
public class UserController {

	@Autowired
	private UserDao dao;
	
	//CREATE
	@PostMapping
	private String create(@RequestBody User user) {
		dao.save(user);
		return "L'utilisateur a été crée";
	}
	
	//READ-ALL
	@GetMapping
	private List<User> readAll(){
		return dao.findAll();
	}
	
	//DELETE
	@DeleteMapping(path = "/{id}")
	private String deleteById(@PathVariable int id) {
		
		dao.deleteById(id);
		
		return "SUCCESS";
	}
}
