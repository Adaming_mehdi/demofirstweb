package com.fr.adaming;

import javax.websocket.server.PathParam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fr.adaming.entity.User;

@SpringBootApplication
@RestController			//SOA ==> REST  , SOAP  (Application Restfull)  [JavaScript Object Notation]
public class DemoFirstWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoFirstWebApplication.class, args);
	}

	@GetMapping(path = "/hello", produces = MediaType.TEXT_HTML_VALUE)
	private String helloWorld() {
		return "<html><head><title>MonAppli</title></head><body><p style=\"color:red\">Hello World From Spring Boot :)</p></body></html>";
	}

	@GetMapping(path = "/hello2", produces = MediaType.TEXT_HTML_VALUE)
	private String helloWorldWithParam(@PathParam("xx") String prenom) {
		return "<html><head><title>MonAppli</title></head><body><p style=\"color:red\">Hello World Mr "+prenom+" From Spring Boot :)</p></body></html>";
	}



	@PostMapping(path = "/hello", produces = MediaType.TEXT_HTML_VALUE)
	private String helloWorldPost() {
		return "<html><head><title>MonAppli</title></head><body><p style=\"color:green\">Hello World From Spring Boot :)</p></body></html>";
	}



	@PostMapping(path = "/register", produces = MediaType.TEXT_HTML_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	private String register(@RequestBody User user) {
		return "<html><head><title>MonAppli</title></head><body><p style=\"color:green\">Vous êtes bien inscrit avec l'email : "+user.getEmail()+"</p></body></html>";
	}
}


// Create ReadAll Delete
